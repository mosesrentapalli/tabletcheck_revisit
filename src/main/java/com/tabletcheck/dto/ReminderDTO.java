package com.tabletcheck.dto;

import java.time.LocalTime;
import java.time.OffsetDateTime;
public class ReminderDTO {
    String remId;
    String usrId;
    LocalTime timeCheck;
    OffsetDateTime createdTimestamp;
    OffsetDateTime expiredTimestamp;

    public String getRemId() {
        return remId;
    }

    public void setRemId(String remId) {
        this.remId = remId;
    }

    public String getUsrId() {
        return usrId;
    }

    public void setUsrId(String usrId) {
        this.usrId = usrId;
    }

    public LocalTime getTimeCheck() {
        return timeCheck;
    }

    public void setTimeCheck(LocalTime timeCheck) {
        this.timeCheck = timeCheck;
    }

    public OffsetDateTime getCreatedTimestamp() {
        return createdTimestamp;
    }

    public void setCreatedTimestamp(OffsetDateTime createdTimestamp) {
        this.createdTimestamp = createdTimestamp;
    }

    public OffsetDateTime getExpiredTimestamp() {
        return expiredTimestamp;
    }

    public void setExpiredTimestamp(OffsetDateTime expiredTimestamp) {
        this.expiredTimestamp = expiredTimestamp;
    }
}
