package com.tabletcheck.entity;

import javax.persistence.*;
import java.sql.Time;
import java.sql.Timestamp;

@Entity
@Table(name = "remindercheck")
public class Reminder {

/*    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;*/
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "reminderid")
    private long reminderId;
    @Column(name = "userid")
    private String userId;
    @Column(name = "timecheck")
    private Time timeCheck;
    @Column(name = "createdtimestamp")
    private Timestamp createdTimestamp;


    public long getRemId() {
        return reminderId;
    }

    public void setRemId(long remId) {
        this.reminderId = remId;
    }

    public String getUsrId() {
        return userId;
    }

    public void setUsrId(String userId) {
        this.userId = userId;
    }

    public Time getTimeCheck() {
        return timeCheck;
    }

    public void setTimeCheck(Time timeCheck) {
        this.timeCheck = timeCheck;
    }

    public Timestamp getCreatedTimestamp() {
        return createdTimestamp;
    }

    public void setCreatedTimestamp(Timestamp createdTimestamp) {
        this.createdTimestamp = createdTimestamp;
    }

    public Timestamp getExpiredTimestamp() {
        return expiredTimestamp;
    }

    public void setExpiredTimestamp(Timestamp expiredTimestamp) {
        this.expiredTimestamp = expiredTimestamp;
    }
    @Column(name = "expiredtimestamp")
    private Timestamp expiredTimestamp;

}
