package com.tabletcheck.service;

import com.tabletcheck.dto.ReminderDTO;
import com.tabletcheck.entity.Reminder;
import com.tabletcheck.repository.ReminderDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Time;
import java.sql.Timestamp;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

@Service
public class ReminderService {

    @Autowired
    ReminderDao reminderDao;
    public String save(ReminderDTO reminderDto) {
        //TODO: use Orika mapping or similar
        Reminder reminder = new Reminder();
        //reminder.setRemId(Long.valueOf(reminderDto.getRemId()));
        reminder.setUsrId(reminderDto.getUsrId());
        reminder.setTimeCheck(Time.valueOf( reminderDto.getTimeCheck() ));
        if(reminderDto.getCreatedTimestamp() == null) {
            reminder.setCreatedTimestamp(Timestamp.valueOf(OffsetDateTime.now().atZoneSameInstant(ZoneOffset.UTC).toLocalDateTime()));

        } else {
            reminder.setCreatedTimestamp(Timestamp.valueOf(reminderDto.getCreatedTimestamp().atZoneSameInstant(ZoneOffset.UTC).toLocalDateTime()));

        }
        if(reminderDto.getExpiredTimestamp() == null) {
            reminder.setExpiredTimestamp(Timestamp.valueOf(OffsetDateTime.now().plusDays(1000).atZoneSameInstant(ZoneOffset.UTC).toLocalDateTime()));

        } else {
            reminder.setExpiredTimestamp(Timestamp.valueOf(reminderDto.getExpiredTimestamp().atZoneSameInstant(ZoneOffset.UTC).toLocalDateTime()));

        }
        // TODO: handle exception
        reminderDao.save(reminder);
        return "Saved";
    }

}
