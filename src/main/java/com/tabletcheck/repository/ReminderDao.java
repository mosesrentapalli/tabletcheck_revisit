package com.tabletcheck.repository;

import com.tabletcheck.entity.Reminder;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReminderDao extends JpaRepository<Reminder,Long> {
}
