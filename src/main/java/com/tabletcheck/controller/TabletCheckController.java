package com.tabletcheck.controller;

import com.tabletcheck.dto.ReminderDTO;
import com.tabletcheck.service.ReminderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/reminder")
public class TabletCheckController {
	@Autowired
	ReminderService reminderService;

	@GetMapping("/")
	public String index() {
		return "Greetings from TabletCHeck";
	}

	@PostMapping(name= "save", consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public String save(@RequestBody ReminderDTO reminder) {
		return reminderService.save(reminder);
	}

}
